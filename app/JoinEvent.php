<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JoinEvent extends Model
{
    //
    protected $table='join_events';

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function event(){
        return $this->belongsTo('App\Event','event_id','id');
    } 
}
