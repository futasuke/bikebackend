<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $table='events';

    public function user(){
        return $this->belongsTo('App\User','owner_id','id');
    }

    public function joinevent(){
        return $this->hasMany('App\JoinEvent','event_id','id');
    }    
}
