<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chatroom extends Model
{
    //
    protected $table='chatrooms';

    public function userone(){
        return $this->belongsTo('App\User','userone_id','id');
    }

    public function usertwo(){
        return $this->belongsTo('App\User','usertwo_id','id');
    }

    public function message(){
        return $this->hasMany('App\Message','chatroom_id','id');
    }
}
