<?php

namespace App\Http\Controllers;

use App\Chatroom;
use App\Comment;
use App\Event;
use App\JoinEvent;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Post;

class ApiController extends Controller
{
    //

    public function userLogin(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!empty($user)) {
            if(Hash::check($request->password,$user->password)){
                $data=array("data"=>$user,"message"=>"Success");
            }else{
                $data=array("errors" => "Password is wrong");
            }
        } else {
            $data=array("errors" => "Email does not exist");
        }
        // if(!empty($user)){

        // }else{
        //     $data[] = [
        //         'error' => "Phone number does not exist"
        //     ];
        // }

        return response()->json($data);
    }

    public function uploadProfileImage(Request $request){
        $user = User::where('id',$request->id)->first();
        $image = $request->input('base64');
        $imageName = "user_".$request->id.".".$request->filetype;
        $imagePath = "storage/profilepic/".$imageName;
        if (Storage::disk('public')->exists("profilepic/".$request->name.".".$request->filetype)) {
            // ...
            Storage::disk('public')->delete("profilepic/".$request->name.".".$request->filetype);
            // return response()->json("Ada file");
        }
        Storage::disk('public')->put("profilepic/".$imageName,base64_decode($image));

        $user->picturePath = $imagePath;
        $user->save();
        return response()->json("Success");
    }

    public function testRelation(){
        // $user = User::with('chatroomuserone','chatroomusertwo','Message')->where('id', 8)->first();
        // dd($user);
        // $post = Post::with('User','Comment')->where('id',1)->first();
        // dd($post);
        // $comment = Comment::with('User','Post')->where('id',1)->first();
        // dd($comment);
        // $event = Event::with('JoinEvent','User')->where('id',1)->first();
        // dd($event);
        // $joinEvent = JoinEvent::with('Event','User')->where('id',1)->first();
        // dd($joinEvent);
        // $chatroom = Chatroom::with('Message','userone','usertwo')->where('id',1)->first();
        // dd($chatroom);
        // $message = Message::with('user','chatroom')->where('id',2)->first();
        // dd($message);
    }
}
